# Hi! , This is the hands-on repo for the meetup

https://www.meetup.com/LE-software-craft-community/events/272788098/


There are two option for the hands on:

1. create a minimal vm, with console only (no GUI) that uses the host's rootfs,
	this option require some [prepareation](preparation.md).

2. use an existing linux machine, (either your host, or whatever vm you already have).
	This require to only install the headers according to your kernel version.
	On Ubuntu this is ``apt install linux-headers-\`uname -r```


If you choose option 2, you don't need the source code, you can still download it
```
git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
```

At each lab directory there is a Makefile.
If you choose option 1 then you should run the make with `AGAINST_SRC=1`:

```
make AGAINST_SRC=1
sudo make AGAINST_SRC=1 modules_install
```
Otherwise without it:
```
make && sudo make modules_install ; sudo depmod
```
To see your installed modules:
```
ls /lib/modules/`uname -r`/extra
```
We will use the kernel log for debugging. You can open a separate terminal and
run `dmesg -w` to constatntly see the new kernel logs.

You can load the kernel module with:
```
sudo modprobe <module file name without .ko extension>
```
for example:
```
sudo modprobe lab0-minimal-kernel-module
```
and unload it with `-r` option:
```
sudo modprobe -r lab0-minimal-kernel-module
```

if `make modules_install` does not work you can try to load the module with:
```
sudo insmod ./lab0-minimal-kernel-module.ko
```
to see list of lab's modules installed:
```
lsmod | grep lab
```

for labs 3, 5 you will have to create a file node for the device:
```
mknod /dev/lab3 c 455 0
```
