#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/slab.h>

#define MY_DEV_NAME "my-device"

#define A_STRING "this is a string"

static int my_probe(struct platform_device *pdev)
{
	char *pd = kzalloc(sizeof(A_STRING), GFP_KERNEL);

	if (!pd)
		return -ENOMEM;
	strscpy(pd, A_STRING, sizeof(A_STRING));
	platform_set_drvdata(pdev, pd);
	dev_dbg(&pdev->dev, "%s: driver probed!!\n", __func__);
	return 0;
}

static int my_remove(struct platform_device *pdev)
{
	char *pd = platform_get_drvdata(pdev);
	dev_dbg(&pdev->dev, "%s: %s!! bye!!\n", __func__, pd);
	kfree(pd);
	return 0;
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static int __init platform_driver_init(void)
{
	int ret;

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret)
		pr_err("%s: error registering the driver (%d)\n", __func__, ret);
	return ret;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_ALIAS("platform:my-device");
MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
