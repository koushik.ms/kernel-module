# TL;DR
## Here are the instructions (with minimal explanations) to prepare for the hands-on on freshly installed ubuntu 20.04.01 (assuming internet was configured)
**Note**,  if your distribution is not ubuntu then you will probably need different instructions.

For any issue/troubleshoot, you can open an ['issue' in the repo](https://gitlab.com/dafnathecool/kernel-module/-/issues)

```
sudo apt update
sudo apt install git make gcc # freshy ubuntu ...

sudo apt install flex bison libelf-dev libssl-dev # some other tools used to compile the kernel
sudo apt install qemu-system-x86

mkdir ~/kernel-module-hands-on
cd ~/kernel-module-hands-on

# this will be ower home directory,
~/kernel-module-hands-on$ mkdir my-home

# this will be ower moduels directory,
~/kernel-module-hands-on$ mkdir modules

# we will use a cool tool called 'virtme'
~/kernel-module-hands-on$ git clone https://github.com/kamomil/virtme.git

# clone the linux source code, this might take few minutes
~/kernel-module-hands-on$ git clone git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git

# 'kbuild' will be the directory of the compiled kernel and object files
~/kernel-module-hands-on$ mkdir kbuild

# the variable $KBUILD_OUTPUT is the directory of the resulted compiled files. It is easier to keep the source code
# and the compiled object files in two separated directories
# Note, if you open another console to keep compiling from there, you should set the KBUILD_OUTPUT var again
# or you can set it in the  ~/.bashrc so to make it automatic
~/kernel-module-hands-on$ export KBUILD_OUTPUT=~/kernel-module-hands-on/kbuild
# enter the linux kernel source code dir
~/kernel-module-hands-on$ cd linux

# this command will configure the kernel adjusted to the hardware of the host, to make it easier to run as a vm
~/kernel-module-hands-on/linux$../virtme/virtme-configkernel --defconfig
...
Configured. Build with 'make ARCH=x86 CROSS_COMPILE=x86_64-linux-gnu- -j8'

# just to make sure, you suppose to have a file called .config in the kbuild directory:
~/kernel-module-hands-on/linux$ ls ../kbuild/.config -l
-rw-r--r-- 1 dafna dafna 129372 Sep 14 19:50 ../kbuild/.config

# the the command 'virtme-configkernel' already told you how to compile: 
~/kernel-module-hands-on/linux$ make ARCH=x86 CROSS_COMPILE=x86_64-linux-gnu- -j$(nproc) # compiling will take 15-30 minutes

# the -jX option tells 'make' to run compilation jobs in parallel. Still you it will take a while (15-30 minutes on a decent laptop)

# here is your kernel:
~/kernel-module-hands-on/linux$ file  $KBUILD_OUTPUT/arch/x86/boot/bzImage
kbuild/arch/x86/boot/bzImage: Linux kernel x86 boot executable bzImage, version 5.9.0-rc5 (dafna@guri) #1 SMP Mon Sep 14 20:19:51 CEST 2020, RO-rootFS, swap_dev 0x9, Normal VGA

# and here are the kernel modules:
~/kernel-module-hands-on/linux$ find $KBUILD_OUTPUT -name "*.ko"

# install the modules:
~/kernel-module-hands-on/linux$ make INSTALL_MOD_PATH=~/kernel-module-hands-on/modules modules_install -j$(nproc)

~/kernel-module-hands-on/linux$ cd ..

# And finally:
~/kernel-module-hands-on$ ./virtme/virtme-run --kdir $KBUILD_OUTPUT --moddir ~/kernel-module-hands-on/modules --mods=none --rwdir my-home/ --cwd my-home/

# At this point you should see your kernel boot log and you are now inside the virtual machine:
root@(none):/home/dafna/kernel-module-hands-on/my-home# uname -a
Linux (none) 5.9.0-rc5 #1 SMP Mon Sep 14 20:19:51 CEST 2020 x86_64 x86_64 x86_64 GNU/Linux

# you are in a virtual machine running the kernel you just compiled!
# if you open another terminal and run:
$ ps aux | grep qemu
~/git/virtme$ ps aux | grep qemu
dafna     4616  0.8  0.4 1193564 160856 pts/3  Sl+  20:58   0:08 /usr/bin/qemu-system-x86_64 -fsdev local,id=virtfs1,path=/,security_model=none,readonly -device virtio-9p-pci,fsdev=virtfs1,mount_tag=/dev/root -fsdev local,id=virtfs5,path=/home/dafna/kernel-module-hands-on/virtme/virtme/guest,security_model=none,readonly -device virtio-9p-pci,fsdev=virtfs5,mount_tag=virtme.guesttools -fsdev local,id=virtfs9,path=my-home/,security_model=none -device virtio-9p-pci,fsdev=virtfs9,mount_tag=virtme.initmount0 -machine accel=kvm:tcg -watchdog i6300esb -cpu host -parallel none -net none -echr 1 -serial none -chardev stdio,id=console,signal=off,mux=on -serial chardev:console -mon chardev=console -vga none -display none -kernel /home/dafna/kernel-module-hands-on/kbuild/arch/x86/boot/bzImage -append virtme_link_mods=/home/dafna/kernel-module-hands-on/kbuild/.virtme_mods/lib/modules/0.0.0 virtme_initmount0=home/dafna/kernel-module-hands-on/my-home earlyprintk=serial,ttyS0,115200 console=ttyS0 psmouse.proto=exps "virtme_stty_con=rows 51 cols 181 iutf8" TERM=xterm-256color virtme_chdir=home/dafna/kernel-module-hands-on/my-home rootfstype=9p rootflags=version=9p2000.L,trans=virtio,access=any raid=noautodetect ro init=/bin/sh -- -c "mount -t tmpfs run /run;mkdir -p /run/virtme/guesttools;/bin/mount -n -t 9p -o ro,version=9p2000.L,trans=virtio,access=any virtme.guesttools /run/virtme/guesttools;exec /run/virtme/guesttools/virtme-init"
dafna     5488  0.0  0.0  14776  1000 pts/7    S+   21:14   0:00 grep --color=auto qemu

# to leave the vm, just enter 'ctrl+a x' (press 'ctrl' and 'a' together, release both and then press 'x')
```


This is the end of the **TL;DR** preparation. If you were successful with the above then you are ready for the hands-on session.

If you have some time, here is the [long explanation](preparation.md).

**See you at the meeting!** : https://www.meetup.com/LE-software-craft-community/events/272788098/


