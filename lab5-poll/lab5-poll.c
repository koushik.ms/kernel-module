/* https://cirosantilli.com/linux-kernel-module-cheat#poll */

#include <linux/errno.h> /* EFAULT */
#include <linux/fs.h>
#include <linux/kernel.h> /* min */
#include <linux/module.h>
#include <linux/poll.h>
#include <linux/printk.h> /* printk */
#include <linux/uaccess.h> /* copy_from_user, copy_to_user */
#include <linux/wait.h> /* wait_queue_head_t, wait_event_interruptible, wake_up_interruptible  */
#include <uapi/linux/stat.h> /* S_IRUSR */
#include <linux/platform_device.h>
#include <linux/cdev.h>

static char c = '_';
static wait_queue_head_t waitqueue;
#define MY_DEV_NAME "my-device"

/*
   The semantics for the copy_to_user() and copy_from_user functions are the same as for the familiar memcpy()
function, except that the return value is the number of bytes not successfully transferred. Thus if we want to transfer
nbytes , the actual value transferred would be:
bytes_transferred = nbytes - copy_to_user (ubuf, kbuf, nbytes);
*/
static ssize_t read(struct file *filp, char __user *buf, size_t len, loff_t *off)
{
	if (copy_to_user(buf, &c, 1))
		return -EFAULT;
	c = '_';
	return 1;
}

static ssize_t write(struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
	if (copy_from_user(&c, buf, 1))
		return -EFAULT;
	
	if (c >= 'A' && c <= 'Z')
		wake_up(&waitqueue);
	return 1;
}

unsigned int poll(struct file *filp, struct poll_table_struct *wait)
{
	poll_wait(filp, &waitqueue, wait);
	if (c >= 'A' && c <= 'Z') {
		pr_info("return POLLIN %c\n", c);
		return POLLIN;
	} else {
		pr_info("return 0\n");
		return 0;
	}
}

static const struct file_operations fops = {
	.owner = THIS_MODULE,
	.read = read,
	.write = write,
	.poll = poll
};

static struct cdev my_cdev;
static unsigned int count = 1;


static int my_major = 500, my_minor = 0;
static int __init myinit(void)
{
	dev_t first;
	int ret;

	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}

	cdev_init(&my_cdev, &fops);
	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		pr_err("%s: cdev_add failed (%d)\n", __func__, ret);
		return ret;
	}

	pr_info("%s: registered on major %u and minor %d\n", __func__, my_major, my_minor);
	init_waitqueue_head(&waitqueue);
	return 0;
}

static void myexit(void)
{
	dev_t first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
}

module_init(myinit)
module_exit(myexit)
MODULE_LICENSE("GPL");
