run:
```
make
make modules_install
```
Then, from the vm you should be able to run:
```
/lib/modules/`uname -r`/extra/lab1-platform-device.ko
dmesg | tail
```

you should see the dmesg prints `register device my-device`
