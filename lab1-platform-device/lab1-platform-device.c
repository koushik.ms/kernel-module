#include <linux/module.h>
#include <linux/platform_device.h>

#define MY_DEV_NAME "my-device"

static void dev_release(struct device *dev)
{
}

static struct platform_device my_device = {
	.name = MY_DEV_NAME,
	.dev.release = dev_release,
};

static int __init platform_device_init(void)
{
	int ret;

	pr_debug("%s: register device %s\n", __func__, MY_DEV_NAME);
	ret = platform_device_register(&my_device);
	if (ret)
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
	return ret;
}

static void __exit platform_device_exit(void)
{
	dev_info(&my_device.dev, "Unregistering Device\n");
	platform_device_unregister(&my_device);
}

module_init(platform_device_init);
module_exit(platform_device_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform device");
MODULE_LICENSE("GPL v2");
