#include <linux/kernel.h> /* for min */
#include <linux/module.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/cdev.h>

#define MY_DEV_NAME "my-device"

#define SIZE 128

struct my_driver {
	struct platform_device* pdev;
};

static DEFINE_MUTEX(my_mutex);
static struct my_driver* mdriver = NULL;

static int mycdrv_open(struct inode *inode, struct file *file)
{
	mutex_lock(&my_mutex);
	/* can mdriver be NULL here ? */
	file->private_data = mdriver ? mdriver->pdev : NULL;
	mutex_unlock(&my_mutex);
	return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
	pr_info("CLOSING device\n");
	file->private_data = NULL;
	return 0;
}

static ssize_t
mycdrv_read(struct file *file, char __user *buf, size_t lbuf, loff_t *ppos)
{
	
	struct platform_device *pd = file->private_data;
	size_t name_len;
	ssize_t nbytes;

	if (!pd)
		return -EINVAL;
	name_len = strlen(pd->name);
        if (*ppos >= name_len)
		return 0;

	name_len = name_len - *ppos;

        nbytes = min(lbuf, name_len) - copy_to_user(buf, pd->name + *ppos, min(lbuf, name_len));
        *ppos += nbytes;
        pr_info("READING function, nbytes=%lu, pos=%llu\n", nbytes, *ppos);
        return nbytes;
}

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .read = mycdrv_read,
        .open = mycdrv_open,
        .release = mycdrv_release
};

static unsigned int count = 1;
static int my_major = 455, my_minor = 0;
static struct cdev my_cdev;

static void free_driver(void)
{
	mutex_lock(&my_mutex);
	if (mdriver) {
		kfree(mdriver);
		mdriver = NULL;
	}
	mutex_unlock(&my_mutex);
}

static int my_probe(struct platform_device *pdev)
{
	static dev_t first;
	int ret;

	dev_dbg(&pdev->dev, "%s: driver probed!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	ret = register_chrdev_region(first, count, MY_DEV_NAME);
	if (ret) {
		pr_err("%s: error registering chrdev region (%d)\n", __func__, ret);
		return ret;
	}
	cdev_init(&my_cdev, &mycdrv_fops);

	mutex_lock(&my_mutex);
	mdriver = kzalloc(sizeof(*mdriver), GFP_KERNEL);
	if (!mdriver) {
		unregister_chrdev_region(first, count);
		return -ENOMEM;
	}
	mdriver->pdev = pdev;
	mutex_unlock(&my_mutex);

	ret = cdev_add(&my_cdev, first, count);
	if (ret) {
		unregister_chrdev_region(first, count);
		free_driver();
		pr_err("%s: cdev_alloc failed (%d)\n", __func__, ret);
		return ret;
	}
	platform_set_drvdata(pdev, mdriver);
	dev_info(&pdev->dev, "%s: driver registered as major %d minor %d\n", __func__, my_major, my_minor);
	return 0;
}

static int my_remove(struct platform_device *pdev)
{
	static dev_t first;

	dev_dbg(&pdev->dev, "%s: bye!!\n", __func__);
	first = MKDEV(my_major, my_minor);
	cdev_del(&my_cdev);
	unregister_chrdev_region(first, count);
	free_driver();
	return 0;
}

static struct platform_driver my_driver = {
	.probe =	my_probe,
	.remove =	my_remove,
	.driver =	{
		.name = MY_DEV_NAME,
	},
};

static int __init platform_driver_init(void)
{
	int ret;

	pr_debug("%s: register driver %s\n", __func__, MY_DEV_NAME);
	ret = platform_driver_register(&my_driver);
	if (ret) {
		pr_err("%s: error registering the device (%d)\n", __func__, ret);
		return ret;
	}
	return 0;
}

static void __exit platform_driver_exit(void)
{
	pr_debug("Unregistering Driver\n");
	platform_driver_unregister(&my_driver);
}

module_init(platform_driver_init);
module_exit(platform_driver_exit);

MODULE_AUTHOR("Dafna");
MODULE_DESCRIPTION("my platform driver");
MODULE_LICENSE("GPL v2");
