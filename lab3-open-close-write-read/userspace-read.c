#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>


#define SIZE 64
char buf[SIZE];

int main(int argc, char *argv[])
{
	ssize_t ret = 0;

	if (argc != 3) {
		printf("usage: %s <dev file name> <read buf size>\n", argv[0]);
		return -1;
	}
	int count = atoi(argv[2]);
	if (count > SIZE || count <= 0) {
		printf("bad buffer size given\n");
		return -1;
	}
	
	int fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror("open faild\n");
		return -1;
	}

	do {
		memset(buf, 0, SIZE);
		ret = read(fd, buf, count);
		if (ret < 0) {
			perror("error reading\n");
			return -1;
		}
		printf("ret = %ld\n", ret);
		printf("%s\n", buf);
	} while (ret);

	printf("done\n");
	return 0;
}
		
	
